# -*- coding: utf-8 -*-
from models import BlockChain, Api
from views import routes
from web import RestMixin
import argparse
import json
import socket
from aiohttp.web import run_app, Application


def init_parser(host="127.0.0.1", port=8080):
    address = f"http://{host}:{port}"
    parser = argparse.ArgumentParser(description="Blockchain Study Project")
    parser.add_argument("--HOST", type=str, nargs="?", default=host)
    parser.add_argument("--PORT", type=int, nargs="?", default=port)
    parser.add_argument("--MAJOR", type=str, nargs="?", default=address)
    return parser


async def init_app(args) -> Application:

    def set_vars(app):
        app["BLOCKCHAIN"] = BlockChain()
        try:
            host = socket.gethostbyname(socket.gethostname())
        except socket.gaierror:
            host = args.HOST
        finally:
            app["ADDRESS"] = f"http://{host}:{args.PORT}"
            app["IS_MAJOR"] = app["ADDRESS"] == args.MAJOR

    async def bind_nodes(app):
        status, resp_json = await RestMixin.call_node(f'{args.MAJOR}{Api.NODES.value}')
        if status == 200:
            node_addresses = resp_json.get("nodes", [])
            _ = [app["BLOCKCHAIN"].nodes.add(address) for address in node_addresses]
            await RestMixin.batch_call(
                node_addresses,
                Api.NODES.value,
                app["ADDRESS"],
                data=json.dumps({'address': app["ADDRESS"]}),
                headers={'Content-Type': 'application/json'}
                )

    app = Application()
    app.add_routes(routes)
    set_vars(app)
    if not(app["IS_MAJOR"]):
        await bind_nodes(app)
    app["BLOCKCHAIN"].nodes.add(app["ADDRESS"])
    return app


if __name__ == "__main__":
    args = init_parser().parse_args()
    run_app(
        init_app(args),
        host=args.HOST,
        port=args.PORT,
        access_log_format="%t  |  %s  |  %a  |  %Tf"
    )
