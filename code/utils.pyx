import hashlib
import json


def hash_block(dict block):
    return hashlib.sha256(json.dumps(block, sort_keys=True).encode()).hexdigest()
