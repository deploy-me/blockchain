# -*- coding: utf-8 -*-
from models import Transaction, Api
from web import RestMixin, catch
import json
import cython
from aiohttp.web import json_response, View, RouteTableDef, Response


routes = RouteTableDef()


@routes.view(Api.CHAIN.value)
class Chain(View, RestMixin):

    async def get(self) -> Response:
        return json_response(self.render_message(
            chain=self.request.app["BLOCKCHAIN"].to_json()
            ))


@routes.view(Api.NODES.value)
class Nodes(View, RestMixin):

    async def get(self) -> Response:
        block_chain = self.request.app["BLOCKCHAIN"]
        if self.request.rel_url.query.get('consensus', None):
            prev_len: cython.int = block_chain.len_chain
            await block_chain.reach_consensus()
            return json_response(self.render_message(
                message="Chain is replaced" if block_chain.len_chain > prev_len else "It's leading chain",
                chain=block_chain.to_json()
                ))
        else:
            return json_response(self.render_message(nodes=list(block_chain.nodes)))

    @catch
    async def post(self) -> Response:
        data = await self.request.json()
        node_address = data.get('address', None)
        if node_address:
            self.request.app["BLOCKCHAIN"].add_node(node_address)
            return json_response(self.render_message(
                message=f"Node {node_address} added"
                ))
        else:
            return json_response(self.render_message(
                message="Node not added"
                ), status=400)


@routes.view(Api.MINE.value)
class Mine(View, RestMixin):

    async def get(self) -> Response:
        block_chain = self.request.app["BLOCKCHAIN"]
        if self.request.app["IS_MAJOR"]:
            success, total = await RestMixin.batch_call(
                block_chain.nodes,
                Api.MINE.value,
                self.request.app["ADDRESS"]
                )
            return json_response(self.render_message(
                message=f'Request success transferred to {success}/{total} nodes'
                ))
        else:
            if len(block_chain.pending_transactions) == 0:
                return json_response(self.render_message(message="No transactions"))
            validated_new_block = block_chain.proof_work(block_chain.block)
            if await block_chain.add_block(validated_new_block):
                return json_response(validated_new_block.to_json())
            else:
                return json_response(self.render_message(
                    message="Block is not valid or other node finished"
                    ), status=400)


@routes.view(Api.TRANSACTIONS.value)
class Transactions(View, RestMixin):

    async def get(self) -> Response:
        return json_response(self.render_message(
            transactions=self.request.app["BLOCKCHAIN"].transactions
            ))

    @catch
    async def post(self) -> Response:
        if self.request.app["IS_MAJOR"]:
            success, total = await RestMixin.batch_call(
                self.request.app["BLOCKCHAIN"].nodes,
                Api.TRANSACTIONS.value,
                self.request.app["ADDRESS"],
                data=json.dumps(await self.request.json()),
                headers={'Content-Type': 'application/json'}
                )
            return json_response(self.render_message(
                message=f'Request success transferred to {success}/{total} nodes'
                ))
        else:
            try:
                transaction = Transaction(**await self.request.json())
            except Exception:
                return json_response(self.render_message(
                    message="Invalid transaction"
                    ), status=400)
            else:
                block_index = self.request.app["BLOCKCHAIN"].add_transaction(transaction)
                return json_response(self.render_message(
                    message=f"Transaction added to block № {block_index}"
                    ))
